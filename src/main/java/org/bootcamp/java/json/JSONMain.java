package org.bootcamp.java.json;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONMain {

    public static void main(String[] args) throws Exception {

        javaToJSON();
        jsonToJava();
    }

    private static void javaToJSON() throws Exception {
        JSONNote jsonNote = new JSONNote();

        Person john = new Person();
        john.setFirstName("John");
        john.setLastName("Doe");
        Person jane = new Person();
        jane.setFirstName("Jane");
        jane.setLastName("Doe");

        jsonNote.setFrom(john);
        jsonNote.setTo(jane);
        jsonNote.setBody("Don't forget me");
        jsonNote.setHeading("Reminder");

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(jsonNote);

        System.out.println(jsonString);
    }

    private static void jsonToJava() throws Exception {
        String json = "{\"to\":{\"firstName\":\"Jane\",\"lastName\":\"Doe\"},\"from\":{\"firstName\":\"John\",\"lastName\":\"Doe\"},\"heading\":\"Reminder\",\"body\":\"Don't forget me\"}\n";

        ObjectMapper objectMapper = new ObjectMapper();
        JSONNote note = objectMapper.readValue(json, JSONNote.class);

        System.out.println(note);
    }

}
