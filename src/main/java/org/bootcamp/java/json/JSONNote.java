package org.bootcamp.java.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONNote {


    private Person to;
    private Person from;
    private String heading;
    private String body;

    public Person getTo() {
        return to;
    }

    public void setTo(Person to) {
        this.to = to;
    }

    public Person getFrom() {
        return from;
    }

    public void setFrom(Person from) {
        this.from = from;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "JSONNote{" +
                "to=" + to +
                ", from=" + from +
                ", heading='" + heading + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
