package org.bootcamp.java.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public class XMLMain {

    public static void main(String[] args) throws Exception {
        javaToXML();
        xmlToJava();
    }

    private static void xmlToJava() throws Exception {

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<note>" +
                "<to>Jane Doe</to>" +
                "<from>John Doe</from>" +
                "<heading>Reminder</heading>" +
                "<body>Don't forget me</body>" +
                "</note>\n";

        //Create JAXBContext and Unmarshaller for JAXB
        JAXBContext context = JAXBContext.newInstance(XMLNote.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        //Reader from where XML will be read
        StringReader sw = new StringReader(xml);


        XMLNote xmlNote = (XMLNote)unmarshaller.unmarshal(sw);
        System.out.println(xmlNote);
    }

    private static void javaToXML() throws Exception {
        //Create Java object
        XMLNote xmlNote = new XMLNote();
        xmlNote.setFrom("John Doe");
        xmlNote.setTo("Jane Doe");
        xmlNote.setBody("Don't forget me");
        xmlNote.setHeading("Reminder");

        //Create JAXBContext and Marshaller for JAXB
        JAXBContext context = JAXBContext.newInstance(XMLNote.class);
        Marshaller marshaller = context.createMarshaller();
        //Writer where to write the result of marshalling n
        StringWriter sw = new StringWriter();
        //Marshal == java->xml
        marshaller.marshal(xmlNote,sw);
        //Now sw (StringWriter) contains XML text made from xmlNote object
        System.out.println(sw.toString());

    }
}
