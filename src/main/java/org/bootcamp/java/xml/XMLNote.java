package org.bootcamp.java.xml;

import javax.xml.bind.annotation.*;

@XmlType(propOrder = {"to","from","heading","body"})
@XmlRootElement(name = "note")
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLNote {

    @XmlElement(required = true)
    private String to;
    @XmlElement(required = true)
    private String from;
    //XMLElement annotation is optional, if default values are ok
    private String heading;
    private String body;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "XMLNote{" +
                "to='" + to + '\'' +
                ", from='" + from + '\'' +
                ", heading='" + heading + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}



